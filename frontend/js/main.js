"use strict";

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie != '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
          var cookie = cookies[i].trim();
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) == (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
          }
      }
  }
  return cookieValue;
}


window.onload = function() {

  // mobile navigation handler
  let navToggle = document.querySelector(".mobile-nav-toggle"),
    nav = document.querySelector("nav")

  if (navToggle && nav) {
    navToggle.addEventListener('click', event => {
      event.preventDefault()
      nav.classList.toggle('is-open')
    })
  }

  // handling of clicking the toplinks in the menu
  let togglers = document.querySelectorAll("#menu .toggler");

  for (let i = 0; i < togglers.length; ++i) {
    const toggler = togglers[i]
    toggler.addEventListener('click', event => {
      event.preventDefault()
      event.stopPropagation();
      let openedToggler = document.querySelector("#menu .toggler.is-open");
      if ( (openedToggler) && (openedToggler != toggler)) {
        openedToggler.classList.remove('is-open');
      }
      toggler.classList.toggle('is-open')
    })
  }

  // close menu if we clicked elsewhere
  window.addEventListener('click', event => {
    let openedToggler = document.querySelector("#menu .toggler.is-open");
    if (openedToggler) {
      openedToggler.classList.remove('is-open');
    }
  })

  var bannerSwiper = new Swiper('.banner-container', {
  	pagination: '.banner-pagination',
    paginationClickable: true,
    nextButton: '.banner-button-next',
    prevButton: '.banner-button-prev',
    spaceBetween: 30,
    // paginationType: 'progress',
    autoplay: 10000,
    autoplayDisableOnInteraction: false,
    loop: true,
  });

  var galleryTop = new Swiper ('.gallery-top', {
    nextButton: '.gallery-button-next',
    prevButton: '.gallery-button-prev',
    spaceBetween: 10,
    preloadImages: false,
    lazyLoading: true
  });

  var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true
  });
  galleryTop.params.control = galleryThumbs;
  galleryThumbs.params.control = galleryTop;

  let galleryCerts = document.querySelector('.gallery-certs')
  if (galleryCerts) {
    let galleryCertsS = new Swiper ('.gallery-certs', {
      nextButton: '.gallery-button-next',
      prevButton: '.gallery-button-prev',
      spaceBetween: 10,
      preloadImages: false,
      lazyLoading: true
    });
  }

  // form submission
  // let csrftoken = getCookie('csrftoken');
  let form = document.querySelector("#contact-form")
  if (form) {
    form.addEventListener('submit', event => {
      event.preventDefault();
      event.stopPropagation();

      let formParent = form.parentNode
      let loader = document.querySelector(".contact-form .loader")

      if (!loader) {
        loader = document.createElement("div")
        loader.setAttribute('class', 'loader')
        let innerDiv = document.createElement("div")
        innerDiv.setAttribute('class', 'loading')
        loader.appendChild(innerDiv)
        formParent.appendChild(loader)
      }

      form.classList.add("closed")

      // AJAXSubmit(form)
      let formData = new FormData(form)
        , postMessage = document.querySelector(".contact-form .postmessage")
        , request = new XMLHttpRequest();

      request.open(form.method, form.action);
      request.onload = () => {
        loader.parentNode.removeChild(loader);
        if (request.status == 500) {
          postMessage.textContent = "Bohužel, nepodařilo s zprávu odeslat."
        }
        postMessage.classList.toggle('closed')
      }
      request.send(formData);
    })

  }

}
