import buble from 'rollup-plugin-buble';
import filesize from 'rollup-plugin-filesize';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import replace from 'rollup-plugin-replace';
import includePaths from 'rollup-plugin-includepaths';
import uglify from 'rollup-plugin-uglify';

let includePathOptions = {
  include: {},
  paths: ['js'],
  external: [],
  extensions: ['.js', '.json', '.html', '.png']
};

export default {
  entry: 'js/main.js',
  format: 'iife',
  // moduleName: "MyMap",
  plugins: [
    includePaths(includePathOptions),
    replace({
      'process.env.NODE_ENV':'"production"',
      '__GAPI_KEY__': '"AIzaSyBIfWYGQOOp4RP0yvW2AuMQXRLr9DCvrDw"'
    }),
    buble(), filesize(), commonjs({
      include: 'node_modules/**'
    }),
    nodeResolve({
      jsnext: true,
      main: true
    }),
    uglify()
  ],
  dest: 'dist/js/main.js'
};
