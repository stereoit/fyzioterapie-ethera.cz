var gulp         = require('gulp')
  , browserSync  = require('browser-sync').create()
  , sass         = require('gulp-sass')
  , notifier 	   = require('node-notifier')
  , sourcemaps   = require('gulp-sourcemaps')
  , postcss      = require('gulp-postcss')
  , autoprefixer = require('autoprefixer')
  , cssnano      = require('cssnano')
  , responsive   = require('gulp-responsive')

var config = {
  dist: 'dist/'
}

function sassErrorHandler (error) {
	notifier.notify({
        title: 'Sass compilation error',
        message: error.message
  	});
	sass.logError.call(this,error);
}

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {
//
//     browserSync.init({
//         server: "./app"
//     });
//
    gulp.watch("scss/**/*.scss", ['sass']);
//     gulp.watch("app/*.html").on('change', browserSync.reload);
});


// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    var processors = [
      autoprefixer({browsers: ['last 1 version']}),
      cssnano(),
    ];

    return gulp.src("scss/*.scss")
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sassErrorHandler))
        .pipe(postcss(processors))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(config.dist + "css"))
        .pipe(browserSync.stream());
});

/* generate responsive gallery images */
gulp.task('gallery', function() {
  return gulp.src('img/gallery/*.jpg')
    .pipe(responsive({
      '*': [{
        // image-medium.jpg is 375 pixels wide
        width: 375,
        rename: {
          extname: '.jpg',
        },
      }, {
        // image-large.jpg is 480 pixels wide
        width: 768,
        rename: {
          suffix: '-medium',
          extname: '.jpg',
        },
      }, {
        // image-extralarge.jpg is 768 pixels wide
        width: 1024,
        rename: {
          suffix: '-large',
          extname: '.jpg',
        },
      }],

    }))
    .pipe(gulp.dest(config.dist + 'img/gallery'));
});

/* generate responsive gallery images */
gulp.task('certs', function() {
  return gulp.src('img/certifikaty/*.{jpg,png}')
    .pipe(responsive({
      '*': [{
        // image-medium.jpg is 375 pixels wide
        width: 375,
        rename: {
          extname: '.jpg',
        },
      }, {
        // image-large.jpg is 480 pixels wide
        width: 768,
        rename: {
          suffix: '-medium',
          extname: '.jpg',
        },
      }, {
        // image-extralarge.jpg is 768 pixels wide
        width: 1024,
        rename: {
          suffix: '-large',
          extname: '.jpg',
        },
      }],

    }))
    .pipe(gulp.dest(config.dist + 'img/certifikaty'));
});

gulp.task('images', ['certs','gallery']);

gulp.task('default', ['serve']);
