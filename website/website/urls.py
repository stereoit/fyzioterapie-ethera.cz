"""website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.views.generic import TemplateView
from .views import contact_form

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^manualni-fyzioterapie-rehabilitace-praha$', TemplateView.as_view(template_name='pages/fyzioterapie.html'), name='fyzioterapie'),
    url(r'^fyzioterapie-lecba-bolesti-laserem$', TemplateView.as_view(template_name='pages/laser.html'), name='laser'),
    url(r'^elektrolecba-lecba-bolesti-praha$', TemplateView.as_view(template_name='pages/elektrolecba.html'), name='elektrolecba'),
    url(r'^fyzioterapeuti-praha-kozakova$', TemplateView.as_view(template_name='pages/terapeuti.html'), name='terapeuti'),
    url(r'^cenik-fyzioterapie-rehabilitace-praha$', TemplateView.as_view(template_name='pages/cenik.html'), name='cenik'),
    url(r'^obchodni-podminky-fyzioterapie$', TemplateView.as_view(template_name='pages/obchodni-podminky.html'), name='obchodni-podminky'),
    url(r'^akutni-bolesti-vyhrezy-vykloubeni-lecba$', TemplateView.as_view(template_name='pages/akutni-bolesti.html'), name='akutni-bolesti'),
    url(r'^chronicke-bolesti-fyzioterapie-zad$', TemplateView.as_view(template_name='pages/chronicke-bolesti.html'), name='chronicke-bolesti'),
    url(r'^fyzioterapie-ploche-nohy-deti$', TemplateView.as_view(template_name='pages/deti.html'), name='deti'),
    url(r'^rehabilitace-sport-bolesti$', TemplateView.as_view(template_name='pages/sport.html'), name='sport'),
    url(r'^typo$', TemplateView.as_view(template_name='typografy.html')),
    url(r'^contact-form-ajax$', contact_form, name='contact-form-ajax'),

    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^admin', include(admin.site.urls)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
