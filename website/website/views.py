from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.core.mail import EmailMessage
from django.conf import settings
import json

from .forms import ContactForm


def contact_form(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            message = form.cleaned_data['message']
            sender_email = form.cleaned_data['email']
            recipients = [settings.DEFAULT_FROM_EMAIL]
            email = EmailMessage(
                "Dotaz z webu " + name,
                message,
                settings.DEFAULT_FROM_EMAIL,
                recipients,
                headers = {'Reply-To': sender_email}
            )
            email.send()
            return JsonResponse({'processed': True})
        return JsonResponse({'errors': 'not valid'})
    else:
        return HttpResponse(
            json.dumps({"nothing to see": "this isn't happening"}),
            content_type="application/json"
        )
