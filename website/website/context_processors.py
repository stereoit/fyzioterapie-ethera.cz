from django.conf import settings

def contact_phone(request):
    return {
        'CONTACT_TELEFON': settings.CONTACT_TELEFON,
        'CONTACT_TELEFON_STRIPPED': settings.CONTACT_TELEFON.strip().replace(" ", "")
        }

def production(request):
    return {
        'PRODUCTION': not settings.DEBUG
    }
