from .base import *

DEBUG=False

STATIC_ROOT = "/srv/static/"
MEDIA_ROOT = "/srv/media/"

SITE_ID = 1

ALLOWED_HOSTS = [
    'fyzioterapie-ethera.cz',
    'www.fyzioterapie-ethera.cz',
    'fyzio.stereoit.com',
    '127.0.0.1',
    'localhost',
]

SERVER_EMAIL = 'info@stereoit.com'

# for email
EMAIL_BACKEND = 'django_ses.SESBackend'
AWS_ACCESS_KEY_ID = get_env_var('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = get_env_var('AWS_SECRET_ACCESS_KEY')
