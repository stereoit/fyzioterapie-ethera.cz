from django import template
from django.utils.safestring import mark_safe
from testimonials.models import Testimonial

register = template.Library()


@register.inclusion_tag('snippets/testimonial.html')
def testimonial():
    try:
        testimonial = Testimonial.objects.order_by("?")[0]
    except IndexError:
        return
    return {
        'testimonial': mark_safe(testimonial.testimonial),
        'client': testimonial.client
    }
