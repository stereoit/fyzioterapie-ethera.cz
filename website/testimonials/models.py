from django.db import models
from tinymce.models import HTMLField


# Create your models here.
class Testimonial(models.Model):
    client = models.CharField(max_length=200, help_text="Klient")
    testimonial = HTMLField(help_text="Recenze")
    created_at = models.DateField(help_text="Datum recenze")
    is_published = models.BooleanField(help_text="Publikováno")

    def __str__(self):
        return self.client

    class Meta:
        get_latest_by = 'is_published'
        ordering = [ '-created_at' ]
